import Vue from 'vue'
import Router from 'vue-router'
import TrainingManagement from '../components/TrainingManagement'
import AwardPunish from '../components/AwardPunish'
import StaffInfo from '../components/StaffInfo'
import DutyScheme from '../components/DutyScheme'
import StaffDuty from '../components/StaffDuty'
import Notice from '../components/Notice'
import Page404 from "../components/Page404";
import Login from "../components/Login";
import Index from "../components/Index"
import Home from "../components/Home";
import HomePage from "../components/HomePage";
import Register from "../components/Register";

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '*',
      name: '404',
      component: Page404
    },{
      path: '/',
      name: 'Home',
      component: Home
    },{
      path:'/Login',
      name:'Login',
      component: Login
    },{
      path:'/Register',
      name:'Register',
      component:Register
    },
    {
      path:'/Index',
      name:'Index',
      component: Index,
      children:[{
          path: '/HomePage',
          name: 'HomePage',
          component: HomePage
        },{
          path: '/TrainingManagement',
          name: 'TrainingManagement',
          component: TrainingManagement
        },{
          path: '/AwardPunish',
          name: 'AwardPunish',
          component: AwardPunish
        },{
          path: '/StaffInfo',
          name: 'StaffInfo',
          component: StaffInfo
        },{
          path: '/DutyScheme',
          name: 'DutyScheme',
          component: DutyScheme
        },{
          path: '/StaffDuty',
          name: 'StaffDuty',
          component: StaffDuty
        },{
          path: '/Notice',
          name: 'Notice',
          component: Notice
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  if(to.name==='Test'){
    alert("无权访问")
    next({name:'HelloWorld'})
  }
  else
    next()
})

export default router
